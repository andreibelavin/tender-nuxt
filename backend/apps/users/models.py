from django.db import models
from django.contrib.auth.models import AbstractUser
from .managers import UserManager


class AbstractCompanyUser(models.Model):

    company_name = models.CharField(
        verbose_name='Название компании', max_length=255, null=True)
    company_inn = models.CharField(
        verbose_name='ИНН', max_length=20, null=True)
    company_kpp = models.CharField(
        verbose_name='ИНН', max_length=20, null=True, blank=True)
    company_ogrn = models.CharField(
        verbose_name='ИНН', max_length=20, null=True, blank=True)
    
    class Meta:
        abstract = True


class AbastractAdditionalInfo(models.Model):

    full_name = models.CharField(verbose_name='Полное имя', max_length=255)
    email = models.EmailField(verbose_name='Почта', max_length=255, null=True, unique=True)
    additional_email = models.EmailField(verbose_name='Доп. почта', max_length=255, null=True, blank=True)
    phone = models.CharField(verbose_name='Телефон', max_length=255, null=True)

    class Meta:
        abstract = True


class User(AbastractAdditionalInfo, AbstractCompanyUser, AbstractUser):

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    username = None
    first_name = None
    last_name = None

    manager = UserManager()
    
    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'

    def __str__(self):
        return self.full_name
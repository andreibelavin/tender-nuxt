from rest_framework import generics
from .serializers.index import *
from .models import IndexSettings


class IndexSettingsAPIView(generics.RetrieveAPIView):
    serializer_class = IndexSettingsSerializer

    def get_queryset(self):
        return IndexSettings.objects.all().prefetch_related('achievments', 'awards', 'activities')

    def get_object(self):
        return self.get_queryset()[0]
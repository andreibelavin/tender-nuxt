# Generated by Django 5.0.3 on 2024-03-16 08:03

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('config', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='achievment',
            name='settings',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='achievments', to='config.indexconf'),
        ),
        migrations.AddField(
            model_name='award',
            name='settings',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='awards', to='config.indexconf'),
        ),
    ]

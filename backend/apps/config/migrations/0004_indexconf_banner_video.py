# Generated by Django 5.0.3 on 2024-03-16 08:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('config', '0003_alter_achievment_options'),
    ]

    operations = [
        migrations.AddField(
            model_name='indexconf',
            name='banner_video',
            field=models.CharField(help_text='Ссылка на youtube или id видео', max_length=255, null=True, verbose_name='Видео'),
        ),
    ]

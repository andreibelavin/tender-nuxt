from django.conf import settings


class HostOverrideMiddleware:

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        request.META['HTTP_X_FORWARDED_HOST'] = settings.OVERRIDE_HOST
        return self.get_response(request)
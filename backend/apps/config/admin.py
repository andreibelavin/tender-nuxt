from django.contrib import admin
from solo.admin import SingletonModelAdmin
from .models import *


class AchievmentInline(admin.TabularInline):
    model = Achievment
    extra = 4
    max_num = 4


class AwardInline(admin.StackedInline):
    model = Award
    extra = 6
    max_num = 6


class ActivityInline(admin.TabularInline):
    model = Activity
    extra = 4
    max_num = 4


class IndexSettingsAdmin(SingletonModelAdmin):
    inlines = (AchievmentInline, AwardInline, ActivityInline,)
    fieldsets = (
        ('Баннер', {
            'fields': ('banner_title', 'banner_preview', 'banner_video'),
        }),
        
        ('Новые тендеры', {
            'fields': ('new_tender_title', 'new_tender_text',),
        }),
        
        ('Награды', {
            'fields': ('awards_title', 'awards_text',),
        }),
        
    )


admin.site.register(IndexSettings, IndexSettingsAdmin)
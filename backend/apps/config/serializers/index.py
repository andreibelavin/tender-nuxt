from ..models.index import *
from rest_framework import serializers


class AchievmentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Achievment
        fields = ('id', 'title', 'value')


class AwardSerialiser(serializers.ModelSerializer):

    class Meta:
        model = Award
        fields = ('id', 'icon', 'text')


class ActivitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Activity
        fields = ('id', 'title', 'image')


class IndexSettingsSerializer(serializers.ModelSerializer):

    achievment_items = AchievmentSerializer(source='achievments', many=True, read_only=True)
    award_items = AwardSerialiser(source='awards', many=True, read_only=True)
    activity_items = ActivitySerializer(source='activities', many=True, read_only=True)

    class Meta:
        model = IndexSettings
        fields = '__all__'
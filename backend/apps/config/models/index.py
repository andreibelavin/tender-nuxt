from django.db import models
from solo.models import SingletonModel


class IndexSettings(SingletonModel):

    banner_title = models.CharField(verbose_name='Заголовок', max_length=255)
    banner_preview = models.ImageField(verbose_name='Превью', upload_to='images/banner')
    banner_video = models.CharField(verbose_name='Видео', max_length=255, help_text='Ссылка на youtube или id видео', null=True)

    new_tender_title = models.CharField(verbose_name='Заголовок', max_length=255)
    new_tender_text = models.TextField(verbose_name='Текст')

    awards_title = models.CharField(verbose_name='Заголовок', max_length=255)
    awards_text = models.TextField(verbose_name='Текст')

    class Meta:
        verbose_name = 'Настройки главной'
        verbose_name_plural = 'Настройки главной'

    def __str__(self):
        return 'Настройки главной'


class Achievment(models.Model):

    settings = models.ForeignKey(IndexSettings, on_delete=models.CASCADE, related_name='achievments', null=True)
    title = models.CharField(verbose_name='Заголовок', max_length=255)
    value = models.CharField(verbose_name='Значение', max_length=255)

    class Meta:
        verbose_name = 'Достижение'
        verbose_name_plural = 'Достижения'

    def __str__(self):
        return self.title


class Award(models.Model):

    settings = models.ForeignKey(IndexSettings, on_delete=models.CASCADE, related_name='awards', null=True)
    icon = models.ImageField(verbose_name='Иконка', upload_to='images/awords-icons/')
    text = models.TextField(verbose_name='Текст')

    class Meta:
        verbose_name = 'Награда'
        verbose_name_plural = 'Награды'

    def __str__(self):
        return self.text


class Activity(models.Model):

    settings = models.ForeignKey(IndexSettings, on_delete=models.CASCADE, related_name='activities', null=True)
    title = models.CharField(verbose_name='Загловок', max_length=255)
    image = models.ImageField(verbose_name='Изображение', upload_to='images/activities/')

    class Meta:
        verbose_name = 'Направление деятельности'
        verbose_name_plural = 'Направления деятельности'

    def __str__(self):
        return self.title
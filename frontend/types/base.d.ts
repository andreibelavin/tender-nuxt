export {};

declare global {

    interface IBreadcrumb {
        title: string,
        link?: string,
    }

    interface IAchievment {
        id: number,
        title: string,
        value: string,
    }

    interface IAward {
        icon: string,
        text: string,
    }

    interface INavItem {
        title: string,
        link: string,
    }

    interface IActivity {
        id: number,
        title: string,
        image: string,
    }
}
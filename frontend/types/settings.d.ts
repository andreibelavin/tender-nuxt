export {};

declare global {
    interface IIndexSettings {
        banner_title: string,
        banner_video: string,
        banner_preview: string,
        achievment_items: IAchievment[],
        new_tender_title: string,
        new_tender_text: string,
        awards_text: string,
        awards_title: string,
        award_items: IAward[],
        activity_items: any,
    }
}
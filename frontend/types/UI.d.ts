export {}

declare global {
    interface IFAQItem {
        id: BigInteger,
        answer: string,
        question: string,
    }
}
import { defineStore } from "pinia";

export const useUserStore = defineStore('user', () => {
    const userToken = ref<string | null>()

    function getToken(){
        if (!userToken)
            userToken.value = localStorage.getItem('userToken')
        return userToken
    }

    function setUserToken(token:string){
        userToken.value = token
        localStorage.setItem('userToken', token)
    }
})
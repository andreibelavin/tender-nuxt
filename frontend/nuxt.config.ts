// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: false },
  modules: [
    '@pinia/nuxt',
  ],
  runtimeConfig: {
    public: {
      apiHost: process.server ?  'http://backend:8000' : 'http://localhost',
    }
  },
  css: [
    '~/assets/scss/additional.scss',
  ],
  devServer: {
    host: '0.0.0.0',
    port: 3000,
  },
  vite: {
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: '@use "~/assets/scss/base.scss" as *;'
        }
      }
    },
    server: {
      watch: {
        usePolling: true,
        interval: 100,

      },
      hmr: {
          clientPort: 24600,
          port: 24600,
          
      },
    }
  },
  
  watchers: {
    webpack: {
      aggregateTimeout: 300,
    }
  }
})
